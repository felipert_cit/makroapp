package com.example.ciandt.makro;

import co.moonmonkeylabs.realmmapview.RealmClusterMapFragment;
import co.moonmonkeylabs.realmsfrestaurantdata.model.Business;

/**
 * Created by fetav on 17/05/2016.
 */
public class BusinessRealmClusterMapFragment extends
        RealmClusterMapFragment<Business> {

    @Override
    protected String getTitleColumnName() {
        return "name";
    }

    @Override
    protected String getLatitudeColumnName() {
        return "latitude";
    }

    @Override
    protected String getLongitudeColumnName() {
        return "longitude";
    }
}